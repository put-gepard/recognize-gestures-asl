import cv2
import numpy as np
import os
import mediapipe as mp

mp_holistic = mp.solutions.holistic
mp_drawing = mp.solutions.drawing_utils

sequ = 30
def detect_landmarks(image, model):
    image.flags.writeable = False
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    results = model.process(image)
    image.flags.writeable = True
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
    return image, results


def extract_keypoints(results):
    right_hand = np.array([[res.x, res.y, res.z] for res in
                           results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(63)
    return right_hand


def capture_camera(folder, path):
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print("Error: Could not open camera.")
        return

    try:
        with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
            for f in folder:
                for seq in range(sequ):
                    for frames in range(30):
                        ret, frame = cap.read()

                        if not ret:
                            print("Error: Failed to capture frame.")
                            break
                        image, results = detect_landmarks(frame, holistic)

                        if results.pose_landmarks:
                            mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)

                        if frames == 0:
                            image = cv2.putText(image, "Start", (00, 185), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255))
                            cv2.putText(image, 'Litera {} numer {}'.format(f, seq),
                                        (15, 12),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                            cv2.imshow('Camera', image)
                            cv2.waitKey(2000)
                        else:
                            cv2.putText(image, 'Litera {} numer {}'.format(f, seq),
                                        (15, 12),
                                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1, cv2.LINE_AA)
                            cv2.imshow('Camera', image)

                        keypoints = extract_keypoints(results)
                        npy_path = os.path.join(path, f, str(seq), str(frames))
                        np.save(npy_path, keypoints)

                        if cv2.waitKey(1) & 0xFF == ord('q'):
                            break

            cap.release()
            cv2.destroyAllWindows()
    finally:
        cap.release()
        cv2.destroyAllWindows()


def saving_input_data(folder, path):
    for f in folder:
        for seq in range(sequ):
            try:
                os.makedirs(os.path.join(path, f, str(seq)))
            except:
                pass


folder = np.array(['A', 'I', 'J'])
data_path = os.path.join("data")

if __name__ == "__main__":
    saving_input_data(folder, data_path)
    capture_camera(folder, data_path)
