from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense
from sklearn import metrics

import numpy as np
import os

letters = np.array(['A','B','C','D','E','F','G','H', 'I', 'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'])
data_path = os.path.join("data")


def create_tests():
    label_map = {label: num for num, label in enumerate(letters)}
    sequences, labels = [], []
    for f in letters:
        for seq in range(30):
            all_frames = []
            for frame in range(30):
                result = np.load(os.path.join(data_path, f, str(seq), "{}.npy".format(frame)))
                all_frames.append(result)
            sequences.append(all_frames)
            labels.append(label_map[f])

    x = np.array(sequences)
    y = to_categorical(labels).astype(int)

    x_tr, x_tt, y_tr, y_tt = train_test_split(x, y, test_size=0.1)
    return x_tr, x_tt, y_tr, y_tt


def neural_network(xtrain, ytrain, xtest, ytest):
    model = Sequential()
    model.add(LSTM(64, return_sequences=True, activation='relu', input_shape=(30, 63)))
    model.add(LSTM(128, return_sequences=True, activation='relu'))
    model.add(LSTM(64, return_sequences=False, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dense(32, activation='relu'))
    model.add(Dense(letters.shape[0], activation='softmax'))

    model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['categorical_accuracy'])
    model.fit(xtrain, ytrain, epochs=2000)

    model.save('my_model.keras')

    predictions = np.argmax(model.predict(xtest), axis=1)
    test_labels = np.argmax(ytest, axis=1)

    accuracy = metrics.accuracy_score(test_labels, predictions)
    print(accuracy)
    print(predictions)


if __name__ == "__main__":
    x_train, x_test, y_train, y_test = create_tests()
    neural_network(x_train, y_train,x_test,y_test)
