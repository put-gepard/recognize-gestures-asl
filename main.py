import mediapipe as mp
import numpy as np
import os
import cv2
import capture_gestures
from tensorflow.keras.models import load_model

letters = np.array(['A','B','C','D','E','F','G','H', 'I', 'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'])
data_path = os.path.join("data")
mp_holistic = mp.solutions.holistic
mp_drawing = mp.solutions.drawing_utils


def live_cam(model):
    cap = cv2.VideoCapture(0)
    sequence = []
    sentence = []
    predictions = []
    last_prediction = []

    if not cap.isOpened():
        print("Error: Could not open camera.")
        return

    try:
        with mp_holistic.Holistic(min_detection_confidence=0.5, min_tracking_confidence=0.5) as holistic:
            while True:
                ret, frame = cap.read()

                image, results = capture_gestures.detect_landmarks(frame, holistic)

                if results.pose_landmarks:
                    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)

                keypoints = capture_gestures.extract_keypoints(results)
                sequence.append(keypoints)

                if len(sequence) == 30:
                    prediction = model.predict(np.expand_dims(sequence, axis=0))[0]

                    predictions.append(np.argmax(prediction))

                    if np.amax(prediction) > 0.9:
                        if last_prediction != letters[np.argmax(prediction)]:
                            sentence.append(letters[np.argmax(prediction)])
                            last_prediction = letters[np.argmax(prediction)]
                    cv2.waitKey(2000)
                    sequence = []
                if len(sentence) > 5:
                    sentence = sentence[-5:]

                cv2.putText(image, ' '.join(sentence), (150, 470),
                            cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

                cv2.imshow('Detect letter', image)
                cv2.waitKey(1)

                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
    finally:
        cap.release()
        cv2.destroyAllWindows()


if __name__ == "__main__":
    model = load_model("my_model.keras")
    live_cam(model)
